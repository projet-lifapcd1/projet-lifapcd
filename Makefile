BUILD_DIR := ./bin
SRC_DIRS := ./src
DOC_DIR := ./doc

CXX := g++
CXXFLAGS := -g -Wall -I src/core -I src/sdl -I src/text
LDFLAGS_SDL = -lSDL2 -lSDL2_image

CORE_MODULES = $(patsubst src/core/%.cpp,bin/core/%.o,$(wildcard src/core/*.cpp))
SDL_MODULES = $(patsubst src/sdl/%.cpp,bin/sdl/%.o,$(wildcard src/sdl/*.cpp))
TEXT_MODULES = $(patsubst src/text/%.cpp,bin/text/%.o,$(wildcard src/text/*.cpp))

TEST_BIN_TARGET = bin/test.o

.PHONY: all sdlgame textgame test
all: sdlgame textgame test
sdlgame: bin/sdlgame
textgame: bin/textgame
test: bin/test

$(BUILD_DIR)/core/%.o: src/core/%.cpp src/core/%.h
	mkdir -p $(BUILD_DIR)/core
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILD_DIR)/sdl/%.o: src/sdl/%.cpp src/sdl/%.h
	mkdir -p $(BUILD_DIR)/sdl
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILD_DIR)/text/%.o: src/text/%.cpp src/text/%.h
	mkdir -p $(BUILD_DIR)/text
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILD_DIR)/sdl/sdlmain.o: src/bin/sdlmain.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILD_DIR)/text/textmain.o: src/bin/textmain.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILD_DIR)/sdlgame: $(CORE_MODULES) $(SDL_MODULES) bin/sdl/sdlmain.o
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LDFLAGS_SDL)

$(BUILD_DIR)/textgame: $(CORE_MODULES) $(TEXT_MODULES) bin/text/textmain.o
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LDFLAGS_SDL)

$(BUILD_DIR)/%.o: src/test/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILD_DIR)/test: $(CORE_MODULES) $(SDL_MODULES) $(TEXT_MODULES) $(TEST_BIN_TARGET)
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LDFLAGS_SDL)

doc:
	mkdir $(DOC_DIR)
	doxygen Doxyfile

clean:
	rm -rf $(BUILD_DIR)
