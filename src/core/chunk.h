#ifndef CHUNK_H
#define CHUNK_H

#include "types.h"
 

//! Représente le type d'un chunk.
enum class Kind { invisible, tree, leaf, main };

//! Encapsule un chunk. Un chunk est un morceaux de terrain. 
class Chunk {
    public:
        //! Initialise un chunk.
        /*!
          \param k Le type du chunk.
          \param pos Sa position.
          \param geometry Sa représentation dans l'espace 2D.
        */
        Chunk(Kind k, Coord pos, Coord geometry);

        //! Initialise un chunk par copie.
        /*!
          \param c Le chunk à partir duquel l'objet est copié.
          \param pos Sa position.
        */
        Chunk(Chunk& c, Coord pos);

        Kind getKind() const;
        Coord getPosition() const;
        Coord getDimension() const;
        Rect getCollision() const;

    protected:
        Kind kind;
        Coord position, geometry;
};

#endif