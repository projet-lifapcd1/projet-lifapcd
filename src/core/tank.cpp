#include "tank.h"

#include <array>
#include <cstdio>
#include <math.h>
#include <utility>

#include "types.h"
#include "missile.h"


array<Direction, 4> allDirection = {Direction::up, Direction::down, Direction::left, Direction::right};

Tank::Tank(bool ennemy, Coord pos, int w, int h) {
    isEnnemy = ennemy;
    x = pos.x;
    y = pos.y;
    width = w;
    height = h;
    targetX = targetY = 0;
    turretX = x + w/2;
    turretY = y + h/2;
    orientation = Direction::right;
    followOrientation = false;
    collisionsState = {};
    exploding = false;
}

Tank::~Tank() {}

bool Tank::update(Collisions cs) {
    // On stocke les collisions pour bloquer les déplacements interdits dans Tank::move.
    collisionsState = cs;

    // On vérifie les collisions pour voir si le tank est touché.
    for (Collision c : collisionsState) {
        if ((isEnnemy && c.first == Collidable::friend_) || c.first == Collidable::danger) {
            exploding = hasIntersection(getCollision(), c.second);
            break;
        }
    }

    // Phase de réflexion de l'IA.
    if (!exploding && isEnnemy) {
        switch (rand()%5) {
        case 0: {
            Tank::move(10, allDirection[rand()%allDirection.size()]);
            break; }
        default:
            break;
        }
    }

    return !exploding;
}

void Tank::move(unsigned int n, Direction dir) {
    followOrientation = (orientation == dir);
    orientation = dir;

    int newX = x, newY = y;
    switch(dir) {
    case Direction::up: newY -= n; break;
    case Direction::down: newY += n; break;
    case Direction::left: newX -= n; break;
    case Direction::right: newX += n; break;
    }

    bool isCollide = false;
    for (Collision c : collisionsState) {
        if (c.first == Collidable::obstacle
            || (isEnnemy && c.first == Collidable::player)
            || (!isEnnemy && c.first == Collidable::ennemy)
            ) {
            if (hasIntersection(make_rect(newX, newY, width, height), c.second)) {
                isCollide = true;
                break;
            }
        }
    }

    if (!isCollide) {
        x = newX;
        y = newY;
        turretX = x + width/2;
        turretY = y + height/2;
    }
}

Missile* Tank::fireMissile() const {
    return (new Missile(!isEnnemy,
                        make_coord(turretX, turretY),
                        make_coord(targetX, targetY)));
}

bool Tank::isAlive() const { return !exploding; }

void Tank::setTargetCoord(Coord coord) {
    targetX = coord.x;
    targetY = coord.y;

    // printf("angle: %f \n", this->getTargetAngle());
}

float Tank::getTargetAngle() const {
    float hypothenus = sqrt(pow(turretX - targetX, 2) + pow(turretY - targetY, 2));
    float ajdacent = targetX - targetY;

    // printf("\n   pos: x: %i, y: %i\n", turretX, turretY);
    // printf("target: x: %i, y: %i\n", targetX, targetY);

    return acos(ajdacent/hypothenus);
}

Rect Tank::getCollision() const {
    return make_rect(x, y, width, height);
}
