#ifndef TANK_H
#define TANK_H

#include <utility>

#include "missile.h"
#include "types.h"


//! Encapsule un tank.
class Tank : public Updatable {
    public:
        //! Initialise un tank.
        /*!
          \param isEnnemy Indique si le tank est un ennemi.
          \param pos La position du tank.
          \param width Sa longueur.
          \param height Sa hauteur.
        */
        Tank(bool isEnnemy, Coord pos, int width, int height);
        ~Tank();

        //! Déplace le tank.
        /*!
          \param n La distance que le tank parcourera.
          \param dir La direction du déplacement.
        */
        void move(unsigned int n, Direction dir);

        //! Tire un missile.
        /*!
          \return Un pointeur vers le missile tiré.
        */
        Missile* fireMissile() const;

        //! Détermine si le tank est en vie.
        bool isAlive() const;

        //! Met à jour les coordonnées du viseur du tank.
        void setTargetCoord(Coord coord);

        //! Récupère l'angle de visé.
        float getTargetAngle() const;

        bool update(Collisions);
        Rect getCollision() const;

    protected:
        int x, y;
        int targetX, targetY;
        int width, height;
        int turretX, turretY;
        Direction orientation;
        bool followOrientation;
        Collisions collisionsState;
        bool isEnnemy, exploding;
};

#endif