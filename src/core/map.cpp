#include "map.h"


Map::Map(Coord ps, vector<Coord> es, vector<Chunk*> cs) {
    playerSpawn = ps,
    ennemySpawns = es;
    chunks = cs;
}

vector<Rect> Map::getCollisions() {
    vector<Rect> collisions;
    for (Chunk* c : chunks) {
        Rect r = c->getCollision();
        if (r != emptyRect) {
            collisions.push_back(r);
        }
    }

    return collisions;
}

Coord Map::getPlayerSpawn() const { return playerSpawn; }
vector<Coord> Map::getEnnemySpawns() const { return ennemySpawns; }
