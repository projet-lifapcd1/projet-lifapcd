#ifndef COMMON_H
#define COMMON_H

#include <ostream>
#include <vector>

#include <SDL2/SDL.h>

#include "types.h"

using namespace std;


//! Représente une collision, son type et sa hitbox.
typedef pair<Collidable, Rect> Collision;

// Pour moins de verbosité.
typedef vector<Collision> Collisions;

//! Une classe abstraite représentant un objet qui peut être mis à jour et qui possède une hitbox.
class Updatable {
    public:
        //! Met à jour l'objet.
        /*!
          \param cs L'état actuel des collisions.
          \return Un booléen indiquant si l'objet a encore besoin d'être mis à jour.
        */
        virtual bool update(Collisions cs) = 0;

        //! Récupère la hitbox de l'objet.
        /*!
          \return Un rectangle représentant la forme que l'objet a dans le plan 2D.
        */
        virtual Rect getCollision() const = 0;
};

//! Une classe abstraite représentant un objet qui peut être rendu, indépendamment de la cible.
template<typename Renderer>
class Renderable {
    public:
        //! Affiche sur l'objet sur la destination de sortie (un écran, un terminal, etc).
        virtual void render(Renderer) const = 0;
};

//! Une classe abstraite utilisée en guise de raccourci pour ne pas avoir à hériter en permanence de Renderer et Updatable
template<typename Renderer>
class UpdatableAndRenderable : public Updatable, public Renderable<Renderer> {};

// On instancie tous les templates
typedef SDL_Renderer* SDLRenderer;
typedef std::ostream& TextRenderer;

class TextRenderable : public Renderable<TextRenderer> {};
class TextUpdatableAndRenderable : public UpdatableAndRenderable<TextRenderer> {};

class SDLRenderable : public Renderable<SDLRenderer> {};
class SDLUpdatableAndRenderable : public UpdatableAndRenderable<SDLRenderer> {};

#endif