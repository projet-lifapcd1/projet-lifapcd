#include "game.h"

#include <vector>

#include "common.h"


template <typename Renderer, typename Map>
Game<Renderer, Map>::Game(Map* m) {
    updatables = {};
    lastCollisionState = {};
    map = m;
    status = GameStatus::in_progress;
}

template <typename Renderer, typename Map>
void Game<Renderer, Map>::track(UpdatableAndRenderable<Renderer>* ur, Collidable c) {
    updatables.push_back(make_pair(c, ur));
}

template <typename Renderer, typename Map>
bool Game<Renderer, Map>::update() {
    Collisions collisions;

    // Collisioons de la map.
    for (Rect r : map->getCollisions()) {
        collisions.push_back(make_pair(Collidable::obstacle, r));
    }

    // On update tous les objets.
    for (long unsigned int i = 0; i < updatables.size(); i++) {
        pair<Collidable, Updatable*> p = updatables[i];
        Updatable* u = p.second;

        if (u->update(lastCollisionState)) {
            Rect r = u->getCollision();
            if (r != emptyRect) {
                collisions.push_back(make_pair(p.first, r));
            }
        } else {
            // L'objet n'a plus besoin d'être mis à jour, on le supprime du vector.
            updatables.erase(updatables.begin() + i);
        }
    }

    lastCollisionState = collisions;

    // La partie est-elle terminée ? 
    checkEnd(collisions);

    return true;
}

template <typename Renderer, typename Map>
void Game<Renderer, Map>::checkEnd(Collisions cs) {
    bool playerIsAlive = false;
    bool ennemiesAlive = false;

    for (Collision c : cs) {
        if (c.first == Collidable::player) {
            playerIsAlive = true;
        } else if (c.first == Collidable::ennemy) {
            ennemiesAlive = true;
        }
    }

    if (!playerIsAlive) status = GameStatus::lose;
    if (!ennemiesAlive) status = GameStatus::win;
}
