#ifndef GAME_H
#define GAME_H

#include <vector>

#include "common.h"
#include "map.h"
#include "sdlmap.h"

using namespace std;


//! Encapsule une partie.
template<typename Renderer, typename Map>
class Game {
    public:
        //! Initialise une partie.
        /*!
          \param m Un pointeur vers la carte où le jeu va se dérouler.
        */
        Game(Map* m);

        //! Déclare qu'un objet doit être entretenu, c'est-à-dire mis-à-jour et afficher à chaque image. 
        /*!
          \param o L'objet à entretenir 
          \param c Un pointeur vers la carte où le jeu va se dérouler.
        */
        void track(UpdatableAndRenderable<Renderer>* o, Collidable c);

        //! Met-à-jour la partie.
        bool update();

    protected:
        vector<pair<Collidable,UpdatableAndRenderable<Renderer>*>> updatables;
        Collisions lastCollisionState;
        Map* map;
        GameStatus status;

        void checkEnd(Collisions);
};

// On instancie les templates.
template class Game<SDLRenderer, SDLMap>;
template class Game<TextRenderer, Map>;

// Quelques alias pour moins de verbosité.
typedef Game<SDLRenderer, SDLMap> SDL_Game;
typedef Game<TextRenderer, Map> Text_Game;

#endif