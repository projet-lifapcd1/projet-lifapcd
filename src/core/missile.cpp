#include "missile.h"


const float SPEED = 0.2; 
const int WIDTH = 20;
const int HEIGHT = 10;

Missile::Missile(bool launched, Coord pos, Coord dir) {
    launchedByPlayer = launched;
    x = pos.x;
    y = pos.y;
    direction = dir;
}

Coord Missile::getPosition() const { return make_coord(x, y); }
Coord Missile::getDirection() const { return direction; }

Rect Missile::getCollision() const {
    return make_rect(x - WIDTH/2, y - HEIGHT/2, WIDTH, HEIGHT);
}

bool Missile::isLaunchedByPlayer() const { return launchedByPlayer; } 

bool Missile::update(Collisions cs) {
    x += SPEED * (direction.x - x);
    y += SPEED * (direction.y - y);

    for (Collision c : cs) {
        if (launchedByPlayer && c.first != Collidable::player) {
            if (hasIntersection(getCollision(), c.second))
                return false;
        }
    }

    return true;
}
