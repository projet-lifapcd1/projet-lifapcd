#include "types.h"

Coord make_coord(int x, int y) { return { x, y }; }

bool Coord::operator==(const Coord& c) const {
    return x == c.x && y == c.y;
}

Rect make_rect(int x, int y, int w, int h) { return { x, y, w, h }; }

bool Rect::operator==(const Rect& r) const {
    return (x == r.x
            && y == r.y
            && width == r.width
            && height == r.height);
}

bool Rect::operator!=(const Rect& r) const {
    return !operator==(r);
}

Rect emptyRect = make_rect(0, 0, 0, 0);

bool hasIntersection(const Rect r1, const Rect r2) {
    int Amin, Amax, Bmin, Bmax;

    // Intersection horizontale.
    Amin = r1.x;
    Amax = Amin + r1.width;
    Bmin = r2.x;
    Bmax = Bmin + r2.width;
    if (Bmin > Amin) Amin = Bmin;
    if (Bmax < Amax) Amax = Bmax;
    if (Amax <= Amin) return false;

    // Intersection verticale.
    Amin = r1.y;
    Amax = Amin + r1.height;
    Bmin = r2.y;
    Bmax = Bmin + r2.height;
    if (Bmin > Amin) Amin = Bmin;
    if (Bmax < Amax) Amax = Bmax;
    if (Amax <= Amin) return false;

    return true;
}