#include "chunk.h"


Chunk::Chunk(Kind k, Coord pos, Coord geom) {
    kind = k;
    position = pos;
    geometry = geom;
}

Chunk::Chunk(Chunk& c, Coord pos) {
    kind = c.kind;
    position = pos;
    geometry = c.geometry;
}

Kind Chunk::getKind() const { return kind; }
Coord Chunk::getPosition() const { return position; }
Coord Chunk::getDimension() const { return geometry; }

Rect Chunk::getCollision() const {
    return make_rect(position.x, position.y, geometry.x, geometry.y);
}
