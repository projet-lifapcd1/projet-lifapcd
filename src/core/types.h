#ifndef TYPES_H
#define TYPES_H

#include <SDL2/SDL.h>


struct Coord {
    int x, y;

    bool operator==(const Coord&) const;
};

Coord make_coord(int x, int y);


//! Représente un rectangle, sa position et ses dimensions.
struct Rect {
    int x, y, width, height;

    bool operator==(const Rect&) const;
    bool operator!=(const Rect&) const;
};

Rect make_rect(int x, int y, int w, int h);

//! Correspond à make_rect(0, 0).
extern Rect emptyRect;

//! Détermine les collisions entre deux rectangles.
/*!
  \return Indique si les deux rectangles se chevauchent.
*/
bool hasIntersection(const Rect r1, const Rect r2);

//! Représente une direction.
enum class Direction { up, down, left, right };

//! Représente le type d'une collision.
enum class Collidable {
    danger, //! Un danger pour le joueur (i.e un missile).
    ennemy, //! Un ennemi.
    friend_, //! Un allié du joueur (i.e un missile que le joueur a tiré).
    obstacle, //! Un obstacle commun au joueur et à aux ennemis.
    player //! Le joueur.
};

//! Représente le type d'une fin de partie.
enum class GameStatus { win, lose, in_progress };

#endif
