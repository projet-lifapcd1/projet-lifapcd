#ifndef MAP_H
#define MAP_H

#include "vector"

#include "chunk.h"
#include "types.h"

using std::vector;


//! Encapsule une carte.
class Map {
    public:
        //! Initialise une carte.
        /*!
          \param playerSpawn Le point d'apparition du joueur.
          \param ennemySpawns Les points d'apparition des ennemis.
          \param chunks L'ensemble de chunks composant la carte.
        */
        Map(Coord playerSpawn, vector<Coord> ennemySpawns, vector<Chunk*> chunks);

        //! Récupère l'ensemble des collisions des objets de la carte.
        vector<Rect> getCollisions();

        Coord getPlayerSpawn() const;
        vector<Coord> getEnnemySpawns() const;

    protected:
        vector<Chunk*> chunks;
        Coord playerSpawn;
        vector<Coord> ennemySpawns;
};

#endif