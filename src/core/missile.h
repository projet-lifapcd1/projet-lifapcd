#ifndef MISSILE_H
#define MISSILE_H

#include "common.h"
#include "types.h"


//! Encapsule un missile.
class Missile : public Updatable {
    public:
        //! Initialise un missile.
        /*
          \param launchedByPlayer Indique si le missile a été tiré par le joueur, ou par un ennemi.
          \param pos La position du missile
          \param dir La direction du missile
        */
        Missile(bool launchedByPlayer, Coord pos, Coord dir);

        Coord getPosition() const;
        Coord getDirection() const;
        Rect getCollision() const;
        bool isLaunchedByPlayer() const;

        bool update(Collisions);

    protected:
        int x, y;
        Coord direction;
        bool launchedByPlayer;
};

#endif
