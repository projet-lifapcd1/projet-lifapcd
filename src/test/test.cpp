#include <cassert>
#include <cstdio>
#include <iostream>

#include "chunk.h"
#include "game.h"
#include "sdltank.h"
#include "texture.h"
#include "textmissile.h"
#include "texttank.h"


int main() {
    // Test chunk.
    Chunk c{Kind::tree, make_coord(0, 0), make_coord(0, 0)};
    assert(c.getKind() == Kind::tree);
    assert(c.getPosition() == make_coord(0, 0));
    assert(c.getDimension() == make_coord(0, 0));
    assert(c.getCollision() == make_rect(0, 0, 0, 0));

    // Test map.
    Chunk c2{Kind::invisible, make_coord(10, 10), make_coord(20, 20)};
    SDLMap m{make_coord(0, 0), {}, {&c, &c2}};
    vector<Rect> cs{make_rect(10, 10, 20, 20)}; 
    assert(m.getCollisions() == cs);
    assert(m.getPlayerSpawn() == make_coord(0, 0));
    vector<Coord> empty;
    assert(m.getEnnemySpawns() == empty);

    // Test game.
    SDL_Game g{&m};
    SDLTank t{false, make_coord(0, 0), NULL, NULL};
    g.track((SDLUpdatableAndRenderable*)&t, Collidable::danger);
    g.track((SDLUpdatableAndRenderable*)&t, Collidable::player);

    // Test missile.
    Missile mi{false, make_coord(50, 100), make_coord(10, 0)};
    assert(mi.getPosition() == make_coord(50, 100));
    assert(mi.getDirection() == make_coord(10, 0));
    assert(mi.getCollision() == make_rect(40, 95, 20, 10));
    mi.update({});
    assert(mi.getPosition() == make_coord(42, 80));
    assert(mi.getDirection() == make_coord(10, 0));

    // Test tank.
    Tank tank{false, make_coord(50, 50), 80, 20};
    assert(tank.getCollision() == make_rect(50, 50, 80, 20));
    tank.move(10, Direction::up);
    assert(tank.getCollision() == make_rect(50, 40, 80, 20));
    tank.move(5, Direction::left);
    assert(tank.getCollision() == make_rect(45, 40, 80, 20));
    assert(tank.isAlive());
    Missile* mi2 = tank.fireMissile();
    assert(mi2->getPosition() == make_coord(85, 50));
    assert(mi2->getDirection() == make_coord(0, 0));
    assert(mi2->getCollision() == make_rect(75, 45, 20, 10));
    tank.setTargetCoord(make_coord(90, 10));
    mi2 = tank.fireMissile();
    assert(mi2->isLaunchedByPlayer());
    assert(mi2->getPosition() == make_coord(85, 50));
    assert(mi2->getDirection() == make_coord(90, 10));

    // Test des rectangles (Rect).
    assert(hasIntersection(make_rect(10, 20, 30, 80), make_rect(20, 50, 20, 15)));
    assert(!hasIntersection(make_rect(60, 70, 10, 30), make_rect(50, 50, 5, 15)));

    // Test SDLTank.
    SDLTank t2{false, make_coord(0, 0), NULL, NULL};
    assert(t2.isAlive());
    SDLMissile* sm = t2.fireMissile();
    assert(sm->getDirection() == make_coord(0, 0));

    // Test texture.
    Texture tex{"/foo/bar"};
    tex.load(NULL);
    assert(tex.get() == NULL);

    // Test TextMissile.
    TextMissile tm{*mi2};
    assert(tm.getCollision() == make_rect(75, 45, 20, 10));
    assert(tm.getPosition() == make_coord(85, 50));
    assert(tm.getDirection() == make_coord(90, 10));

    // Test TextTank.
    TextTank t3{false, make_coord(75, 75)};
    assert(t3.isAlive());
    assert(t3.getCollision() == make_rect(75, 75, 90, 90));

    printf("Test passé avec succès.");

    return 0;
}