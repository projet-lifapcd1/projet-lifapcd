#ifndef SDLMAP_H
#define SDLMAP_H

#include "vector"

#include "common.h"
#include "map.h"
#include "texture.h"

using std::vector;


//! Encapsule une carte évoluant dans un environnement graphique.
class SDLMap : public Map, public SDLRenderable {
    public:
        //! Voir Map::Map().
        SDLMap(Coord, vector<Coord>, vector<Chunk*>);

        void setTexture(Texture*);

        void render(SDLRenderer) const;
    private:
        Texture* tilemap;
};

#endif