#ifndef SDLMISSILE_H
#define SDLMISSILE_H

#include <SDL2/SDL.h>

#include "missile.h"
#include "texture.h"


//! Encapsule un missile évoluant dans un environnement graphique.
class SDLMissile : public Missile, public SDLUpdatableAndRenderable {
    public:
        //! Initialise par copie un missile.
        /*!
          \param m Un objet Missile.
          \param t La texture du missile.
        */
        SDLMissile(Missile& m, Texture* t);

        Rect getCollision() const;
        bool update(Collisions);
        void render(SDLRenderer) const;

    private:
        bool explosing;
        long unsigned int explosionCurrentFrame;
        Texture* tilemap;
};

#endif
