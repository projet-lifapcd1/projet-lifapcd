#ifndef SDLTANK_H
#define SDLTANK_H

#include "tank.h"

#include <SDL2/SDL.h>

#include "sdlmissile.h"
#include "texture.h"

//! Encapsule un tank évoluant dans un environnement graphique.
class SDLTank : public Tank, public SDLUpdatableAndRenderable {
    public:
        //! Initialise un tank.
        /*!
          \param isEnnemy Voir Tank::Tank().
          \param pos Voir Tank::Tank().
          \param tankTilemap Un pointeur vers la texture contenant les sprites du tank.
          \param explosionTilemap Un pointeur vers la texture contenant les sprites d'explosions.
        */
        SDLTank(bool isEnnemy, Coord pos, Texture* tankTilemap, Texture* explosionTilemap);

        //! Voir Tank::move.
        void move(unsigned int, Direction);

        //! Voir Tank::fireMissile.
        SDLMissile* fireMissile() const;

        //! Voir Tank::isAlive.
        bool isAlive() const;

        Rect getCollision() const;

        bool update(Collisions);
        void render(SDLRenderer) const;

    private:
        int spriteW, spriteH;
        Texture* tilemap;
        Texture* tilemapExplosion;
        int currentFrame, explosionCurrentFrame;
        bool exploding, dead;

        void renderSprite(SDL_Renderer*) const;
        void renderGunTurretSprite(SDL_Renderer*) const;
        void renderTargetSprite(SDL_Renderer*) const;
};

#endif
