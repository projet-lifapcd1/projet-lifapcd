#ifndef SDLGAME_H
#define SDLGAME_H

#include <SDL2/SDL.h>

#include "game.h"
#include "sdlmap.h"


//! Encapsule une partie se déroulant dans un environnement graphique.
class SDLGame : public SDL_Game, public SDLRenderable {
    public:
        //! Voir Game::Game().
        SDLGame(SDLMap* m);
        ~SDLGame();

        SDL_Renderer * renderer;

        void render(SDLRenderer) const;

    private:
        SDL_Window* win;
        Texture* winTexture;
        Texture* loseTexture;
};

#endif