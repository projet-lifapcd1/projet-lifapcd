#include "sdlmissile.h"

#include <array>
#include <cassert>

#include <SDL2/SDL_image.h>


const int EXPLOSION_DST_WIDTH = 150;
const int SPRITE_SRC_WIDTH = 88;

std::array<Coord, 28> explosionFrames = {
    make_coord(0, 0), make_coord(89, 0), make_coord(178, 0), make_coord(267, 0), make_coord(356, 0), make_coord(445, 0), make_coord(534, 0),
    make_coord(0, 89), make_coord(89, 89), make_coord(178, 89), make_coord(267, 89), make_coord(356, 89), make_coord(445, 89), make_coord(534, 89),
    make_coord(0, 178), make_coord(89, 178), make_coord(178, 178), make_coord(267, 178), make_coord(356, 178), make_coord(445, 178), make_coord(534, 178),
    make_coord(0, 267), make_coord(89, 267), make_coord(178, 267), make_coord(267, 267), make_coord(356, 267), make_coord(445, 267), make_coord(534, 267),
};

SDLMissile::SDLMissile(Missile& m, Texture* tex) : Missile(m.isLaunchedByPlayer(), m.getPosition(), m.getDirection())  {
    explosing = false;
    explosionCurrentFrame = 0L;
    tilemap = tex;
}

Rect SDLMissile::getCollision() const {
    return Missile::getCollision();
}

bool SDLMissile::update(Collisions cs) {
    if (explosing) {
        if (explosionCurrentFrame == explosionFrames.size() - 1) {
            // L'animation de l'explosion est terminée, plus besoin de mettre à jour l'objet.
            explosionCurrentFrame = 0L;
            explosing = false;
            return false;
        } else {
            // On met à jour le sprite de l'explosion.
            explosionCurrentFrame++;
            return true;
        }
    } else {
        if (Missile::update(cs)) {
            // Le missile a encore besoin d'être mis-à-jour : il n'est pas en train d'exploser
            return true;
        } else {
            // Le missile a atteint sa position finale. On déclenche l'explosion.
            explosing = true;
            return true;
        }
    }

    assert(false);
}

void SDLMissile::render(SDLRenderer renderer) const {
    if (explosing) {
        Coord spriteCoord = explosionFrames[explosionCurrentFrame];
        SDL_Rect srcRect{spriteCoord.x, spriteCoord.y, SPRITE_SRC_WIDTH, SPRITE_SRC_WIDTH};
        SDL_Rect destRect{x - EXPLOSION_DST_WIDTH/2,
                          y - EXPLOSION_DST_WIDTH/2,
                          EXPLOSION_DST_WIDTH, EXPLOSION_DST_WIDTH};

        SDL_RenderCopy(renderer, tilemap->get(), &srcRect, &destRect);
    } else {
        int width = 20;
        int height = 10;
        SDL_Rect rect = {x - width/2, y - height/2, width, height};
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderDrawRect(renderer, &rect);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    }
}
