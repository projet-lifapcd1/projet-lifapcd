#include "sdltank.h"

#include <array>
#include <cassert>
#include <cmath>

#include <SDL2/SDL_image.h>


const int TANK_WIDTH = 90;
const int SPRITE_SRC_WIDTH = 48; // Taille d'un sprite sur la tilemap.
const int GUN_TURRET_SRC_WIDTH = 32; // Taille du sprite de la tourelle sur la tilemap.
const int TARGET_SRC_WIDTH = 48;
const int TARGET_DST_WIDTH = 48;
const int MOVE_FRAMES = 3;

const int EXPLOSION_DST_WIDTH = 150;
const int EXPLOSION_SPRITE_SRC_WIDTH = 128;
const int EXPLOSION_SPRITE_SRC_HEIGHT = 128;

// Coordonnées sur la tilemap des frames d'explosion.
std::array<Coord, 33> explosionSpriteFrames = {
    make_coord(387, 1363), make_coord(516, 1363), make_coord(645, 1363),
    make_coord(0, 1508), make_coord(129, 1508), make_coord(258, 1508), make_coord(387, 1508), make_coord(516, 1508), make_coord(645, 1508),
    make_coord(0, 1653), make_coord(129, 1653), make_coord(258, 1653), make_coord(387, 1653), make_coord(516, 1653), make_coord(645, 1653),
    make_coord(0, 1798), make_coord(129, 1798), make_coord(258, 1798), make_coord(387, 1798), make_coord(516, 1798), make_coord(645, 1798),
    make_coord(0, 1944), make_coord(129, 1944), make_coord(258, 1944), make_coord(387, 1944), make_coord(516, 1944), make_coord(645, 1944),
    make_coord(0, 2088), make_coord(129, 2088), make_coord(258, 2088), make_coord(387, 2088), make_coord(516, 2088), make_coord(645, 2088),
};

SDLTank::SDLTank(bool isEnnemy, Coord pos, Texture* tex, Texture* texEx) : Tank(isEnnemy, pos, TANK_WIDTH, TANK_WIDTH) {
    spriteW = spriteH = SPRITE_SRC_WIDTH;
    tilemap = tex;
    tilemapExplosion = texEx;
    currentFrame = 0;
    explosionCurrentFrame = 0;
    exploding = false;
    dead = false;
}

void SDLTank::move(unsigned int n, Direction dir) {
    Tank::move(n, dir);

    // Mis-à-jour du sprite.
    if (followOrientation) {
        currentFrame += 1;
        currentFrame %= MOVE_FRAMES;
    } else {
        currentFrame = 0;
    }
}

SDLMissile* SDLTank::fireMissile() const {
    Missile * m = Tank::fireMissile();
    return new SDLMissile(*m, tilemapExplosion);
}

bool SDLTank::isAlive() const {
    return !dead;
}

Rect SDLTank::getCollision() const {
    return Tank::getCollision();
}

bool SDLTank::update(Collisions cs) {
    if (exploding) {
        if (explosionCurrentFrame == explosionSpriteFrames.size() - 1) {
            explosionCurrentFrame = 0L;
            exploding = false;
            dead = true;
            return false;
        } else {
            explosionCurrentFrame++;
            return true;
        }
    } else {
        if (Tank::update(cs)) {
            int x, y;
            SDL_GetMouseState(&x, &y);
            Tank::setTargetCoord(make_coord(x, y));

            return true;
        } else {
            exploding = true;

            return true;
        }
    }
}

Coord getTidleMapSpriteCoord(Direction orientation) {
    switch (orientation) {
        case Direction::up:
            return make_coord(1, 373);
        case Direction::down:
            return make_coord(299, 373);
        case Direction::left:
            return make_coord(448, 373);
        case Direction::right:
            return make_coord(150, 373);
    }

    // Ne peut pas arriver : le case ci-dessus est exhaustif.
    assert(false);
}

void SDLTank::renderTargetSprite(SDL_Renderer* renderer) const {
    SDL_Rect srcRect{529, 779, TARGET_SRC_WIDTH, TARGET_SRC_WIDTH};
    SDL_Rect destRect{targetX - TARGET_DST_WIDTH/2,
                      targetY - TARGET_DST_WIDTH/2,
                      TARGET_DST_WIDTH,
                      TARGET_DST_WIDTH};

    SDL_RenderCopy(renderer, tilemap->get(), &srcRect, &destRect);
}

void SDLTank::renderSprite(SDL_Renderer* renderer) const {
    const Coord coord = getTidleMapSpriteCoord(orientation);
    SDL_Rect srcRect{coord.x + currentFrame*(SPRITE_SRC_WIDTH + 1),
                     coord.y,
                     spriteW,
                     spriteH};
    SDL_Rect destRect{x, y, height, width};

    SDL_RenderCopy(renderer, tilemap->get(), &srcRect, &destRect);
}

// Il y a 32 sprites pour modéliser les différentes positions de la tourelles.
// Un tour complet = 360° donc 360/32 = 11.5 et les sprites sont ajustés tous
// les 11.5°.

void SDLTank::renderGunTurretSprite(SDL_Renderer* renderer) const {
    SDL_Rect srcRect{1, 812, GUN_TURRET_SRC_WIDTH, GUN_TURRET_SRC_WIDTH}; // 256, 779
    SDL_Rect destRect{x + width/3/2, y + height/3/2, height/3*2, width/3*2};

    SDL_RenderCopy(renderer, tilemap->get(), &srcRect, &destRect);
}

void SDLTank::render(SDLRenderer renderer) const {
    if (exploding) {
        Coord spriteCoord = explosionSpriteFrames[explosionCurrentFrame];
        SDL_Rect srcRect{spriteCoord.x,
                         spriteCoord.y,
                         EXPLOSION_SPRITE_SRC_WIDTH,
                         EXPLOSION_SPRITE_SRC_HEIGHT};
        SDL_Rect destRect{x - EXPLOSION_DST_WIDTH/2,
                          y - EXPLOSION_DST_WIDTH/2,
                          EXPLOSION_DST_WIDTH, EXPLOSION_DST_WIDTH};

        SDL_RenderCopy(renderer, tilemapExplosion->get(), &srcRect, &destRect);
    } else {
        this->renderSprite(renderer);
        this->renderGunTurretSprite(renderer);
        this->renderTargetSprite(renderer);
    }

    // Dessin d'un segment entre le viseur et la tourelle.
    // SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    // SDL_RenderDrawLine(renderer, x + 45, y + 45, targetX, targetY);
    // SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}
