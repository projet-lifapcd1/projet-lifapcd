#include "texture.h"
#include <iostream>


Texture::Texture(string path) {
    filename = path;
    texture = NULL;
}

Texture::Texture(SDL_Renderer* renderer, string path) : Texture(path) {
    this->load(renderer);
}

Texture::~Texture() {
    if (texture != NULL) {
        SDL_DestroyTexture(texture);
    }
}

void Texture::load(SDL_Renderer* renderer) {
    if (texture == NULL) {
        texture = IMG_LoadTexture(renderer, filename.c_str());
    }
}

SDL_Texture* Texture::get() { return texture; } 
