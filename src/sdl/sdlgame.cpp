#include "sdlgame.h"

#include <vector>

#include <SDL2/SDL.h>

#include "common.h"
#include "game.h"
#include "meta.h"


SDLGame::SDLGame(SDLMap* m) : Game(m) {
    win = SDL_CreateWindow(game_name.c_str(),
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED,
                            DIMW,
                            DIMH,
                            0);
    renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
    SDL_ShowCursor(SDL_DISABLE);

    winTexture = new Texture(renderer, "data/win.png");
    loseTexture = new Texture(renderer, "data/defeat.png");
}

SDLGame::~SDLGame() {
    delete winTexture;
    delete loseTexture;
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(win);
}

void SDLGame::render(SDLRenderer renderer) const {
    SDL_RenderClear(renderer);

    // On affiche les objets de la map.
    map->render(renderer);

    // Puis tous les autres objets.
    for (pair<Collidable, Renderable*> p : updatables) {
        p.second->render(renderer);
    }

    // En cas de fin de partie, on affiche l'écran de fin.
    if (status == GameStatus::win) {
        SDL_Rect destRect{DIMW/2-250, DIMH/2-250, 500, 500};
        SDL_RenderCopy(renderer, winTexture->get(), NULL, &destRect);
    } else if (status == GameStatus::lose) {
        SDL_Rect destRect{DIMW/2-250, DIMH/2-250, 500, 500};
        SDL_RenderCopy(renderer, loseTexture->get(), NULL, &destRect);
    }

    SDL_RenderPresent(renderer);
}