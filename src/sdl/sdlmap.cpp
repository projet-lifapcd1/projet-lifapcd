#include "sdlmap.h"

#include <cassert>


const int MAIN_TILE_WIDTH = 50;
const int MAIN_TILE_SPRITE_WIDTH = 16;

SDLMap::SDLMap(Coord ps, vector<Coord> es, vector<Chunk*> cs) : Map(ps, es, cs) {
    tilemap = nullptr;
};

void SDLMap::setTexture(Texture* tex) {
    tilemap = tex;
}

Rect getTidleMapSpriteCoord(Kind k) {
    switch (k) {
        case Kind::tree: return make_rect(0, 0, 31, 37);
        case Kind::leaf: return make_rect(32, 0, 16, 16);
        case Kind::main: return make_rect(49, 0, 16, 16);
        case Kind::invisible: return emptyRect;
    }

    // Ne peut pas arriver : le case ci-dessus est exhaustif.
    assert(false);
}

void SDLMap::render(SDLRenderer renderer) const {
    if (tilemap != nullptr) {
        int width, height;
        SDL_GetRendererOutputSize(renderer, &width, &height);

        for (int x = 0; x < width ; x+=MAIN_TILE_WIDTH) {
            for (int y = 0; y < height; y+=MAIN_TILE_WIDTH) {
                SDL_Rect srcRect{49, 0, MAIN_TILE_SPRITE_WIDTH, MAIN_TILE_SPRITE_WIDTH};
                SDL_Rect destRect{x, y, MAIN_TILE_WIDTH, MAIN_TILE_WIDTH};
                SDL_RenderCopy(renderer, tilemap->get(), &srcRect, &destRect);
            }
        }
    }

    for (Chunk* c : chunks) {
        if (c->getKind() != Kind::invisible) {
            Rect src = getTidleMapSpriteCoord(c->getKind());
            SDL_Rect srcRect{src.x, src.y, src.width, src.height};

            Coord dstDim = c->getDimension();
            Coord chunkPos = c->getPosition();
            SDL_Rect destRect{chunkPos.x, chunkPos.y, dstDim.x, dstDim.y};

            // Affiche les hitbox "brutes".
            // SDL_RenderDrawRect(renderer, &destRect);

            SDL_RenderCopy(renderer, tilemap->get(), &srcRect, &destRect);
        }
    }
}