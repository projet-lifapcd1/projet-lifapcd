#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>

#include <SDL2/SDL_image.h>

using std::string;


//! Encapsule un wrapper autour des textures SDL.
class Texture {
    public:
        //! Initialise une texture.
        /*!
          \param path Le chemin de fichier de l'image.
        */
        Texture(string path);

        //! Initialise une texture.
        /*!
          \param r L'objet SDL renderer dans lequel charger la texture.
          \param path Voir Texture::Texture().
        */
        Texture(SDL_Renderer* r, string path);

        ~Texture();

        //! Charge la texture.
        /*!
          \param r L'objet SDL renderer dans lequel charger la texture.
        */
        void load(SDL_Renderer* r);

        SDL_Texture* get();

    private:
        string filename;
        SDL_Texture * texture;
};

#endif
