#include <ctime> 

#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_image.h>

#include "sdlgame.h"
#include "sdlmap.h"
#include "sdlmissile.h"
#include "sdltank.h"
#include "texture.h"


const int DIMW = 1000;
const int DIMH = 1000;
const int CHUNKW = DIMW/2/6;
const int CHUNKH = DIMH/2/5;

Chunk borderUp = Chunk(Kind::invisible, make_coord(0, -1), make_coord(DIMW, 1));
Chunk borderDown = Chunk(Kind::invisible, make_coord(0, DIMH), make_coord(DIMW, 1));
Chunk borderLeft = Chunk(Kind::invisible, make_coord(-1, 0), make_coord(1, DIMH));
Chunk borderRight = Chunk(Kind::invisible, make_coord(DIMW, 0), make_coord(1, DIMH));

Chunk tree = Chunk(Kind::tree, make_coord(0, 0), make_coord(CHUNKW, 100));
Chunk leaf = Chunk(Kind::leaf, make_coord(0, 0), make_coord(CHUNKW*0.75, CHUNKW*0.75));

// Le premier L.
Chunk t1 = Chunk(tree, make_coord(DIMW/2 + CHUNKW, CHUNKH*2));
Chunk t2 = Chunk(tree, make_coord(DIMW/2 + CHUNKW, CHUNKH*3));
Chunk t3 = Chunk(tree, make_coord(DIMW/2 + CHUNKW, CHUNKH*4));
Chunk t4 = Chunk(tree, make_coord(DIMW/2 + CHUNKW*2, CHUNKH*4));
Chunk t5 = Chunk(tree, make_coord(DIMW/2 + CHUNKW*3, CHUNKH*4));
// Le second L.
Chunk t6 = Chunk(tree, make_coord(CHUNKW*2, DIMH/2));
Chunk t7 = Chunk(tree, make_coord(CHUNKW*3, DIMH/2));
Chunk t8 = Chunk(tree, make_coord(CHUNKW*4, DIMH/2));
Chunk t9 = Chunk(tree, make_coord(CHUNKW*4, DIMH/2 + CHUNKH));
Chunk t10 = Chunk(tree, make_coord(CHUNKW*4, DIMH/2 + CHUNKH*2));

Chunk l1 = Chunk(leaf, make_coord(CHUNKW*1.5, 150));
Chunk l2 = Chunk(leaf, make_coord(DIMW - CHUNKW*1.5 - leaf.getDimension().x, DIMH - 150 - leaf.getDimension().y));

vector<Coord> ennemySpawns = {make_coord(DIMW/2 + CHUNKW*3, CHUNKH*2)};
SDLMap map = SDLMap(make_coord(CHUNKW*2, DIMH/2 + CHUNKH*2),
                    ennemySpawns,
                    {&borderUp, &borderDown, &borderLeft, &borderRight,
                     &t1, &t2, &t3, &t4, &t5,
                     &t6, &t7, &t8, &t9, &t10,
                     &l1, &l2});

int main() {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        printf("Error initializing SDL: %s\n", SDL_GetError());
        return 1;
    }
    srand((unsigned)time(0)); 

    SDLGame game = SDLGame(&map);

    Texture mapTex = Texture(game.renderer, "data/map_sprites.png");
    map.setTexture(&mapTex);

    Texture missileTex = Texture(game.renderer, "data/missile_sprites.png");
    Texture tankTex = Texture(game.renderer, "data/tank_sprites.png");

    SDLTank player = SDLTank(false, map.getPlayerSpawn(), &tankTex, &missileTex);
    game.track((SDLUpdatableAndRenderable*)&player, Collidable::player);

    vector<SDLTank> ennemies;
    for (Coord spawn : map.getEnnemySpawns()) {
        SDLTank ennemy = SDLTank(true, spawn, &tankTex, &missileTex);
        ennemies.push_back(ennemy);
        game.track((SDLUpdatableAndRenderable*)&ennemy, Collidable::ennemy);
    }

    bool running = true;
    while (running) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.scancode) {
                case SDL_SCANCODE_ESCAPE:
                    running = false;
                    break;
                case SDL_SCANCODE_W:
                case SDL_SCANCODE_UP:
                    player.move(10, Direction::up);
                    break;
                case SDL_SCANCODE_A:
                case SDL_SCANCODE_LEFT:
                    player.move(10, Direction::left);
                    break;
                case SDL_SCANCODE_S:
                case SDL_SCANCODE_DOWN:
                    player.move(10, Direction::down);
                    break;
                case SDL_SCANCODE_D:
                case SDL_SCANCODE_RIGHT:
                    player.move(10, Direction::right);
                    break;
                case SDL_SCANCODE_SPACE: {
                    SDLMissile * m = player.fireMissile();
                    game.track((SDLUpdatableAndRenderable*)m, Collidable::friend_);
                    break; }
                default:
                    break;
                };
                break;
            default:
                break;
            }
        }

        game.update();
        game.render(game.renderer);

        SDL_Delay(1000 / 60);
    }

    SDL_Quit();

    return 0;
}
