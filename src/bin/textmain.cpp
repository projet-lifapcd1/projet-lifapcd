#include <iostream>

#include "map.h"
#include "textgame.h"
#include "texttank.h"
#include "textmissile.h"

using std::cout;
using std::endl;
using std::string;


const int DIMW = 1000;
const int DIMH = 1000;
const int CHUNKW = DIMW/2/6;
const int CHUNKH = DIMH/2/5;

Chunk borderUp = Chunk(Kind::invisible, make_coord(0, -1), make_coord(DIMW, 1));
Chunk borderDown = Chunk(Kind::invisible, make_coord(0, DIMH), make_coord(DIMW, 1));
Chunk borderLeft = Chunk(Kind::invisible, make_coord(-1, 0), make_coord(1, DIMH));
Chunk borderRight = Chunk(Kind::invisible, make_coord(DIMW, 0), make_coord(1, DIMH));

Chunk tree = Chunk(Kind::tree, make_coord(0, 0), make_coord(CHUNKW, 100));
Chunk leaf = Chunk(Kind::leaf, make_coord(0, 0), make_coord(CHUNKW*0.75, CHUNKW*0.75));

// Le premier L.
Chunk t1 = Chunk(tree, make_coord(DIMW/2 + CHUNKW, CHUNKH*2));
Chunk t2 = Chunk(tree, make_coord(DIMW/2 + CHUNKW, CHUNKH*3));
Chunk t3 = Chunk(tree, make_coord(DIMW/2 + CHUNKW, CHUNKH*4));
Chunk t4 = Chunk(tree, make_coord(DIMW/2 + CHUNKW*2, CHUNKH*4));
Chunk t5 = Chunk(tree, make_coord(DIMW/2 + CHUNKW*3, CHUNKH*4));
// Le second L.
Chunk t6 = Chunk(tree, make_coord(CHUNKW*2, DIMH/2));
Chunk t7 = Chunk(tree, make_coord(CHUNKW*3, DIMH/2));
Chunk t8 = Chunk(tree, make_coord(CHUNKW*4, DIMH/2));
Chunk t9 = Chunk(tree, make_coord(CHUNKW*4, DIMH/2 + CHUNKH));
Chunk t10 = Chunk(tree, make_coord(CHUNKW*4, DIMH/2 + CHUNKH*2));

Chunk l1 = Chunk(leaf, make_coord(CHUNKW*1.5, 150));
Chunk l2 = Chunk(leaf, make_coord(DIMW - CHUNKW*1.5 - leaf.getDimension().x, DIMH - 150 - leaf.getDimension().y));

vector<Coord> ennemySpawns = {make_coord(DIMW/2 + CHUNKW*3, CHUNKH*2)};
Map map = Map(make_coord(CHUNKW*2, DIMH/2 + CHUNKH*2),
              ennemySpawns,
              {&borderUp, &borderDown, &borderLeft, &borderRight,
                  &t1, &t2, &t3, &t4, &t5,
                  &t6, &t7, &t8, &t9, &t10,
                  &l1, &l2});

int main() {
    srand((unsigned)time(0));

    TextGame game = TextGame(&map);

    TextTank player = TextTank(false, map.getPlayerSpawn());
    game.track((TextUpdatableAndRenderable*)&player, Collidable::player);

    vector<TextTank> ennemies;
    for (Coord spawn : map.getEnnemySpawns()) {
        TextTank ennemy = TextTank(true, spawn);
        ennemies.push_back(ennemy);
        game.track((TextUpdatableAndRenderable*)&ennemy, Collidable::ennemy);
    }

    cout << "Welcome on Companion of Rage" << endl;

    bool running = true;
    while (running) {
        string event;
        getline(std::cin, event);
        // Raccourci pour tester si l'évènement est un mouvement.
        char firstch = (event.size() == 1) ? tolower(event[0]) : '\0';

        if (firstch == 'z') {
            player.move(10, Direction::up);
        } else if (firstch == 'q') {
            player.move(10, Direction::left);
        } else if (firstch == 's') {
            player.move(10, Direction::down);
        } else if (firstch == 'd') {
            player.move(10, Direction::right);
        } else if (firstch == ' ') {
            Missile * m = player.fireMissile();
            TextMissile* missile = new TextMissile(*m);
            game.track((TextUpdatableAndRenderable*)missile, Collidable::friend_);
        } else if (event == "quit") {
            running = false;
        } else {
            cout << "Touche inconnue" << endl;
        }

        game.update();
        game.render(cout);
    }

    return 0;
}
