#include "textgame.h"

#include <cstdio>


TextGame::TextGame(Map* map) : Game(map) {}

TextGame::~TextGame() {}

void TextGame::render(TextRenderer out) const {
    for (pair<Collidable, Renderable*> p : updatables) {
        p.second->render(out);
    }

    if (status == GameStatus::win) {
        printf("You have win!");
        exit(0);
    } else if (status == GameStatus::lose) {
        printf("You have lose!");
        exit(0);
    }
}