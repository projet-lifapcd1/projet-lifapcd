#ifndef TEXTMISSILE_H
#define TEXTMISSILE_H

#include "missile.h"


//! Encapsule un missile évoluant dans un environnement textuel.
class TextMissile : public Missile, public TextUpdatableAndRenderable {
    public:
        //! Initialise par copie un missile.
        /*!
          \param m Un objet Missile.
        */
        TextMissile(Missile& m);

        Rect getCollision() const;
        bool update(Collisions);
        void render(TextRenderer) const;
};

#endif
