#ifndef TEXTTANK_H
#define TEXTTANK_H

#include "tank.h"


//! Encapsule un tank évoluant dans un environnement textuel.
class TextTank : public Tank, public TextUpdatableAndRenderable {
    public:
        //! Voir Tank::Tank().
        TextTank(bool, Coord);
        ~TextTank();

        Rect getCollision() const;

        bool update(Collisions);
        void render(TextRenderer) const;
};

#endif
