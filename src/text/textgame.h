#ifndef TEXTGAME_H
#define TEXTGAME_H

#include <ostream>

#include "game.h"


//! Encapsule une partie se déroulant dans un environnement textuel.
class TextGame : public Text_Game, public TextRenderable {
    public:
        //! Voir Game::Game().
        TextGame(Map*);
        ~TextGame();

        void render(TextRenderer) const;
};

#endif