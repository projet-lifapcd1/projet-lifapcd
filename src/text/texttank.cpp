#include "texttank.h"

#include <iostream>

using std::endl;


const int TANK_WIDTH = 90;

TextTank::TextTank(bool isEnnemy, Coord pos) : Tank(isEnnemy, pos, TANK_WIDTH, TANK_WIDTH) {}

TextTank::~TextTank() {}

Coord getCursorPosition() {
    #ifdef _WIN32
        #include <windef.h>

        POINT p;
        GetCursorPos(&p);
        return make_coord(p.x, p.y);
    #else
        return make_coord(0, 0);
    #endif
}

Rect TextTank::getCollision() const {
    return Tank::getCollision();
}

bool TextTank::update(Collisions cs) {
    this->setTargetCoord(getCursorPosition());

    return Tank::update(cs);
}

void TextTank::render(TextRenderer out) const {
    out << (isEnnemy ? "Ennemi" : "Joueur") << ":" << endl;
    out << "  Position: (" << x << ", " << y << ")" << endl;
    out << "  Cible: (" << targetX << ", " << targetY << ")" << endl;
}