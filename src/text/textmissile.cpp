#include "textmissile.h"

using std::endl;


TextMissile::TextMissile(Missile& m) : Missile(m.isLaunchedByPlayer(), m.getPosition(), m.getDirection()) {}

Rect TextMissile::getCollision() const {
    return Missile::getCollision();
}

bool TextMissile::update(Collisions cs) { return Missile::update(cs); }

void TextMissile::render(TextRenderer out) const {
    out << "Missile:" << endl;
    out << "  Position: (" << x << ", " << y << ")" << endl;
}
