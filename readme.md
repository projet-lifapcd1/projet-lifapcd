
# Companion of Rage

[Homepage](https://forge.univ-lyon1.fr/projet-lifapcd1/projet-lifapcd)

## Auteurs

- Yanice Kirati (p2203139)
- Timéo Arnouts (p2202913)

## Manuel

### Description du jeu

Il s'agit d'un jeu de tank en 1 contre 1. Le but du jeu est de détruire les tanks adverses.

#### Mode graphique

Utilisez `zqsd` pour vous déplacer, `Espace` pour tirer et la souris pour déplacer le viseur.

#### Mode textuel

Utilisez `zqsd` ou la touche `Espace` puis `Entrer` pour vous déplacer et tirer via le prompt interactif.
La commande `quit` permet de quitter le jeu.

### Compilation

Pour construire les deux exécutables, graphique et textuel :
```bash
make
```
Pour constuire individuellement chaque exécutable et le lancer :
```bash
make sdl && bin/sdlgame # Pour SDL
make text && bin/textgame # Pour l'interface textuelle.
```

#### Test

Pour lancer les tests :
```bash
make test && bin/test
```

**Note :** Nous n'avons pas jugé bon d'implémenter une méthode `void test()` directement dans chaque classe.
À la place, nous avons choisi de tester chaque classe individuellement dans le fichier [test/test.cpp](src/test/test.cpp).

### Structure du projet

- `bin/` : contient les exécutables ;
- `data/` : contient les fichiers nécessaires au jeu ;
- `doc/` : contient la documentation du projet ;
- `src/` : contient les sources :
  * `bin/` : des binaires pour l'interface graphique et textuelle ;
  * `core/` : communes à l'interface graphique et textuelle ;
  * `sdl/` : de l'interface graphique utilisant SDL ;
  * `test/` : des tests ;
  * `text/` : de l'interface textuelle.

## Documentation

Pour construire la documentation dans le repertoire `./doc/html` :
```bash
make doc
```

**Note :** Nous avons considéré qu'il n'y avait pas besoin de documenter tous les _getter_ et _setter_, leurs noms explicites et signatures parlent d'eux-mêmes.
